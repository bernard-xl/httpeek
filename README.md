# HTTPeek
Peek into HTTPS connections!

This is a proof of concept demonstrating how to intercept HTTPS traffic by performing a MITM (Man in the Middle) attack.

## How does it work?
This article may save me thousands of word: [https://docs.mitmproxy.org/stable/concepts-howmitmproxyworks/](How mitmproxy works)

## Environment to run this proof of concept
- Ubuntu 20.04
- Go >= 1.18

## How to run it?
```bash
# On shell #1
# Install self-signed CA cert on local machine, start the proxy server.
git clone https://gitlab.com/bernard-xl/httpeek
cd httpeek
script/setup.sh
go run main.go

# On shell #2
# Sending a request to https://example.com via localhost:8088 as proxy.
curl -v -x http://localhost:8088 https://example.com
```

## The result
We can see the proxy successfully dump out the requests and responses passing through it.
![Server logs](asset/server-side.png)

The client side successfully verify the fake TLS cert.
![Client logs](asset/client-side.png)