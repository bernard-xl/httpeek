#! /usr/bin/env bash

set -v
set -e

go install github.com/cloudflare/cfssl/cmd/...@latest

mkdir -p pki
cd pki

cfssl genkey -initca root-csr.json | cfssljson -bare root
cfssl genkey leaf-csr.json | cfssljson -bare leaf
cfssl sign -ca root.pem -ca-key root-key.pem -csr leaf.csr | cfssljson -bare leaf

sudo cp root.pem /usr/local/share/ca-certificates/local.crt
sudo update-ca-certificates
