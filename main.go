package main

import (
	"bufio"
	"context"
	"crypto/tls"
	"flag"
	"io"
	"log/slog"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/signal"
)

type Config struct {
	ListenAddress string
	TLSCertPath   string
	TLSKeyPath    string
}

type Proxy struct {
	TLSConfig tls.Config
}

func (p *Proxy) PassthroughMode(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodConnect {
		w.WriteHeader(http.StatusBadGateway)
		return
	}

	if r.URL.Host == "" {
		slog.Error("invalid tunnel initiation", "cause", "empty host in request URL")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if r.URL.Port() == "" {
		slog.Error("invalid tunnel initiation", "cause", "port is unspecified in request URL")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	slog.Info("establishing a tunnel", "destination", r.RequestURI)

	hj, ok := w.(http.Hijacker)
	if !ok {
		slog.Error("hijacking is unsupported on the connection")
		w.WriteHeader(http.StatusBadGateway)
		return
	}

	w.WriteHeader(http.StatusOK)
	upConn, rw, err := hj.Hijack()
	if err != nil {
		slog.Error("failed to hijack connection", "cause", err.Error())
		return
	}

	downConn, err := net.Dial("tcp", r.RequestURI)
	if err != nil {
		slog.Error("failed to connect to downstream", "destination", r.RequestURI)
		upConn.Close()
		return
	}

	slog.Info("tunnel established", "destination", r.RequestURI)

	go func() {
		defer downConn.Close()
		io.Copy(downConn, rw)
	}()
	go func() {
		defer upConn.Close()
		io.Copy(rw, downConn)
	}()
}

func (p *Proxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodConnect {
		w.WriteHeader(http.StatusBadGateway)
		return
	}

	if r.URL.Host == "" {
		slog.Error("invalid tunnel initiation", "cause", "empty host in request URL")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if r.URL.Port() == "" {
		slog.Error("invalid tunnel initiation", "cause", "port is unspecified in request URL")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	slog.Info("establishing a tunnel", "destination", r.RequestURI)

	hj, ok := w.(http.Hijacker)
	if !ok {
		slog.Error("hijacking is unsupported on the connection")
		w.WriteHeader(http.StatusBadGateway)
		return
	}

	upConn, _, err := hj.Hijack()
	if err != nil {
		slog.Error("failed to hijack connection", "err", err)
		return
	}

	termConn := tls.Server(upConn, &p.TLSConfig)

	_, err = upConn.Write([]byte("HTTP/1.1 200 OK\r\n\r\n"))
	if err != nil {
		slog.Error("failed to confirm tunnel establishment", "err", err)
		termConn.Close()
		return
	}

	go func() {
		defer termConn.Close()
		termReader := bufio.NewReader(termConn)

		for {
			passReq, err := http.ReadRequest(termReader)
			if err != nil {
				if err == io.EOF {
					slog.Info("connection closed")
					break
				}
				slog.Error("failed to read http request from upstream", "err", err)
				break
			}

			reqDump, _ := httputil.DumpRequest(passReq, true)
			if len(reqDump) != 0 {
				slog.Info("passthrough request", "dump", string(reqDump))
			}

			passReq.URL, err = url.Parse("https://" + passReq.Host + passReq.RequestURI)
			if err != nil {
				slog.Info("invalid forwarding url", "err", err)
				break
			}
			passReq.RequestURI = ""

			passResp, err := http.DefaultClient.Do(passReq)
			if err != nil {
				slog.Error("failed to connect to downstream", "err", err)
				break
			}

			respDump, _ := httputil.DumpResponse(passResp, true)
			if len(respDump) != 0 {
				slog.Info("passthrough response", "dump", string(respDump))
			}

			err = passResp.Write(termConn)
			if err != nil {
				slog.Error("broken connection with upstream", "err", err)
			}
			passResp.Body.Close()
		}
	}()
}

func main() {
	var cfg Config
	flag.StringVar(&cfg.ListenAddress, "listen", ":8088", "Listen address of the proxy")
	flag.StringVar(&cfg.TLSCertPath, "cert", "pki/leaf.pem", "Path to TLS certificate")
	flag.StringVar(&cfg.TLSKeyPath, "key", "pki/leaf-key.pem", "Path to TLS private key")
	flag.Parse()

	cert, err := tls.LoadX509KeyPair(cfg.TLSCertPath, cfg.TLSKeyPath)
	if err != nil {
		slog.Error("failed to load server TLS certificate and private key", "err", err)
		os.Exit(1)
	}

	proxy := &Proxy{
		TLSConfig: tls.Config{
			Certificates: []tls.Certificate{cert},
		},
	}

	server := http.Server{
		Addr:    cfg.ListenAddress,
		Handler: proxy,
	}

	shutdownC := make(chan struct{})
	signalC := make(chan os.Signal, 1)
	signal.Notify(signalC, os.Interrupt)
	go func() {
		<-signalC
		server.Shutdown(context.Background())
		close(shutdownC)
	}()

	slog.Info("start listening", "address", cfg.ListenAddress)

	err = server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		slog.Error("failed to listen and serve", "err", err)
		os.Exit(1)
	}
	<-shutdownC
}
